const images = [
	"mountains-1.jpg",
	"mountains-2.jpg",
	"lake.jpg",
	"lighthouse.jpg",
	"south-tyrol.jpg",
	"sunset.jpg"
];

let currentImageIndex = 0;
let previousBtn = document.getElementById("previousBtn");
let nextBtn = document.getElementById("nextBtn");
let currentImage = document.getElementById("currentImage");

previousBtn.addEventListener("click", showPreviousImage);
nextBtn.addEventListener("click", showNextImage);

function showPreviousImage() {
	currentImageIndex = (currentImageIndex - 1 + images.length) % images.length;
	currentImage.src = "./images/" + images[currentImageIndex];
}

function showNextImage() {
	currentImageIndex = (currentImageIndex + 1) % images.length;
	currentImage.src = "./images/" + images[currentImageIndex];
}
